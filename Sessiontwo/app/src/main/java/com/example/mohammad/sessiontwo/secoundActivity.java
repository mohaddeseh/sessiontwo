package com.example.mohammad.sessiontwo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class secoundActivity extends AppCompatActivity {


    TextView studentNmae;
    Button btnShow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secound);

        studentNmae = (TextView) findViewById(R.id.studentName);
        btnShow = (Button) findViewById(R.id.btnShow);
        studentNmae.setText("Mohadeseh");


        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                studentNmae.setText("Hello World");

                Toast.makeText(secoundActivity.this, "clicked on BTN SHOW", Toast.LENGTH_SHORT).show();
            }
        });

        btnShow.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                Toast.makeText(secoundActivity.this, "onLong click", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        studentNmae.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnShow.setText("Alireza");
            }
        });


    }
}
